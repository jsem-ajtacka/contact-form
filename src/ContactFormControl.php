<?php

declare(strict_types=1);

namespace JsemAjtacka\ContactForm;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Localization\Translator;

class ContactFormControl extends Control
{
    /**
     * @var callable
     */
    public $onSuccess;

    public function __construct(private ?string $templateFile, private Translator $translator) {}

    public function render(): void
    {
        if (is_null($this->templateFile)) {
            $this->templateFile = __DIR__ . "/templates/form.latte";
        }

        $this->template->setFile($this->templateFile);
        $this->template->render();
    }

    public function createComponentForm(): Form
    {
        $form = new Form();

        $form->setTranslator($this->translator);

        $form->addText("name", "contactForm.name.label")
            ->setHtmlAttribute("placeholder", "contactForm.name.placeholder")
            ->setRequired("contactForm.validations.name.required");

        $form->addText("email", "contactForm.email.label")
            ->setHtmlAttribute("placeholder", "contactForm.email.placeholder")
            ->setRequired("contactForm.validations.email.required");

        $form->addTextArea("message", "contactForm.message.label")
            ->setHtmlAttribute("placeholder", "contactForm.message.placeholder")
            ->setRequired("contactForm.validations.message.required");

        $form->addSubmit("submit", "contactForm.submit.label");

        $form->onSuccess = $this->onSuccess;

        return $form;
    }
}