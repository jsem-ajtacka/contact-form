<?php

declare(strict_types=1);

namespace JsemAjtacka\ContactForm;

use Nette\Localization\Translator;

class ContactFormFactory
{
    public function __construct(private ?string $templateFile, private Translator $translator) {}

    public function create(): ContactFormControl {
        return new ContactFormControl($this->templateFile, $this->translator);
    }
}