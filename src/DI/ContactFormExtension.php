<?php

declare(strict_types=1);

namespace JsemAjtacka\ContactForm\DI;

use JsemAjtacka\ContactForm\ContactFormFactory;
use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;

final class ContactFormExtension extends CompilerExtension
{
    public function getConfigSchema(): Schema
    {
        return Expect::structure([
            'templateFile' => Expect::string()
        ]);
    }

    public function loadConfiguration(): void
    {
        $templateFile = $this->config->templateFile;

        $this->getContainerBuilder()
            ->addDefinition($this->prefix('factory'))
            ->setFactory(ContactFormFactory::class)
            ->setArgument('templateFile', $templateFile);
    }
}