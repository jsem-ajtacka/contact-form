# Contact Form extension for Nette Framework

This is a simple Nette Framework extension for Contact Form.
Contact form consists of these fields: `name`, `email`, `message`. It also tracks the `sentAt` time and offers boolean status `done` for administration of (un)read messages.

## Installation

Use Composer to install this extension:

```
$ composer reguire jsemajtacka/contact-form
```

## Configuration

Add this to your configuration .neon file:

```
extensions:
    contactForm: JsemAjtacka\ContactForm\DI\ContactFormExtension

contactForm:
    contactFormService: Path\To\Your\Text\Block\Service\Class
    template: Path\To\Your\Latte\Template
```

The `template` argument is optional.

## Example of usage

`BasePresenter.php:`

```php
class BasePresenter extends Nette\Application\UI\Presenter
{
    #[Inject]
    public ContactFormFactory $contactFormFactory;

    protected function createComponentContactForm(): ContactFormControl
    {
        return $this->contactFormFactory->create();
    }
}
```

`@layout.latte:`
```latte
{control contactForm}
```